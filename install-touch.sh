mkdir -p /usr/share/lomiri-session || true
mkdir -p /usr/share/wayland-sessions || true
mkdir -p /usr/share/lightdm/sessions || true

cp run-systemd-session /usr/share/lomiri-session/
cp lomiri.service /usr/lib/systemd/user
cp lomiri-session /usr/bin

cp touch/usc-wrapper /usr/share/lomiri-session/
cp touch/lomiri-touch-session /usr/bin
cp touch/lomiri-touch.desktop /usr/share/lightdm/sessions
cp touch/52-lomiri-touch.conf /usr/share/lightdm/lightdm.conf.d
