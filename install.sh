mkdir -p /usr/lib/lomiri-session || true
mkdir -p /usr/share/wayland-sessions || true

cp run-systemd-session /usr/lib/lomiri-session/
cp lomiri.service /usr/lib/systemd/user
cp lomiri-session /usr/bin

cp desktop/dm-lomiri-session /usr/bin
cp desktop/lomiri.desktop /usr/share/wayland-sessions
